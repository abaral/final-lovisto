from django.db import models
from django.contrib.auth.models import User


class Aportacion(models.Model):

    VOTOS = [('L', 'like'),
             ('D', 'dislike'),
             ('N', 'nulo')]
    titulo = models.CharField(max_length=100)
    enlace = models.TextField()
    descripcion = models.TextField()
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)
    extend_info = models.TextField()
    num_coments = models.IntegerField(default=0)
    likes = models.ManyToManyField(User, related_name='likes', blank=True)
    dislikes = models.ManyToManyField(User, related_name='dislikes', blank=True)
    voto = models.CharField(max_length=1, choices=VOTOS, default='N')

    def __str__(self):
        return str(self.id) + ' ' + str(self.titulo)


class Comentario(models.Model):
    aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE)
    cuerpo = models.TextField()
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.aportacion) + ' ' + str(self.cuerpo) + ' ' + str(self.autor) + ' ' + str(self.fecha)
