from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib.request

extend_info = ""
days_whether_data = ""
pageid = ""


class CounterHandlerWiki(ContentHandler):

    def __init__(self):
        self.in_content = False
        self.in_page = False
        self.content = ""
        self.titulo = ""
        self.first_chars = ""

    def startElement(self, name, attrs):
        global pageid
        if name == 'page':
            pageid = attrs.get('pageid')
            self.titulo = attrs.get('title')
            self.in_page = True
        elif name == 'extract':
            self.in_content = True

    def endElement(self, name):
        global extend_info
        if name == 'page':
            self.in_page = False
        elif name == 'extract':
            self.first_chars = self.content
            self.content = ''
            self.in_content = False
            extend_info = "<div class=\"wiki\">Artículo " + self.titulo + \
                          " de Wikipedia.<p>" + self.first_chars[:400] + "...</p><p>Copyright de Wikipedia</p></div>"

    def characters(self, chars):
        if self.in_content:
            self.content = self.content + chars


class CounterHandlerAemet(ContentHandler):

    def __init__(self):
        self.in_content = False
        self.in_day = False
        self.in_temp = False
        self.in_sens_term = False
        self.in_hum_rel = False
        self.content = ""
        self.municipio = ""
        self.provincia = ""
        self.max_temp = ""
        self.min_temp = ""
        self.max_sens_term = ""
        self.min_sens_term = ""
        self.max_hum_rel = ""
        self.min_hum_rel = ""
        self.fecha = ""

    def startElement(self, name, attrs):
        if name == 'nombre':
            self.in_content = True
        elif name == 'provincia':
            self.in_content = True
        elif name == 'dia':
            self.fecha = attrs.get('fecha')
            self.in_day = True
        elif self.in_day:
            if name == 'temperatura':
                self.in_temp = True
            elif self.in_temp:
                if name == 'maxima':
                    self.in_content = True
                elif name == 'minima':
                    self.in_content = True
            elif name == 'sens_termica':
                self.in_sens_term = True
            elif self.in_sens_term:
                if name == 'maxima':
                    self.in_content = True
                elif name == 'minima':
                    self.in_content = True
            elif name == 'humedad_relativa':
                self.in_hum_rel = True
            elif self.in_hum_rel:
                if name == 'maxima':
                    self.in_content = True
                elif name == 'minima':
                    self.in_content = True

    def endElement(self, name):
        global extend_info
        global days_whether_data
        if name == 'nombre':
            self.municipio = self.content
            self.content = ''
            self.in_content = False
        elif name == 'provincia':
            self.provincia = self.content
            self.content = ''
            self.in_content = False
        elif name == 'dia':
            days_whether_data = days_whether_data \
                                + "<li> Día: " + self.fecha + ". Temperaturas ºC (min, max): (" + self.min_temp \
                                + ", " + self.max_temp + ") Sensación térmica ºC (min, max): (" \
                                + self.min_sens_term + ", " + self.max_sens_term \
                                + ") Humedad relativa (min, max): (" + self.min_hum_rel + ", " \
                                + self.max_hum_rel + ")</li>"
            self.in_day = False
            self.content = ''
            self.in_content = False
        elif self.in_day:
            if name == 'temperatura':
                self.in_temp = False
            elif self.in_temp:
                if name == 'maxima':
                    self.max_temp = self.content
                    self.content = ''
                    self.in_content = False
                elif name == 'minima':
                    self.min_temp = self.content
                    self.content = ''
                    self.in_content = False
            elif name == 'sens_termica':
                self.in_sens_term = False
            elif self.in_sens_term:
                if name == 'maxima':
                    self.max_sens_term = self.content
                    self.content = ''
                    self.in_content = False
                elif name == 'minima':
                    self.min_sens_term = self.content
                    self.content = ''
                    self.in_content = False
            elif name == 'humedad_relativa':
                self.in_hum_rel = False
            elif self.in_hum_rel:
                if name == 'maxima':
                    self.max_hum_rel = self.content
                    self.content = ''
                    self.in_content = False
                elif name == 'minima':
                    self.min_hum_rel = self.content
                    self.content = ''
                    self.in_content = False
        elif name == 'root':
            extend_info = "<div class=\"aemet\">Información sobre el clima en " + self.municipio \
                          + " (" + self.provincia + "):" \
                          + "<ul>" + days_whether_data + "</ul></div>"

    def characters(self, chars):
        if self.in_content:
            self.content = self.content + chars


def main(url_xml, sitio):
    global pageid
    xml_file = urllib.request.urlopen(url_xml)
    parser = make_parser()
    if sitio == 'aemet':
        handler = CounterHandlerAemet()
    elif sitio == 'wiki':
        handler = CounterHandlerWiki()
    parser.setContentHandler(handler)
    parser.parse(xml_file)
    if sitio == 'aemet':
        return extend_info
    elif sitio == 'wiki':
        return extend_info, pageid
