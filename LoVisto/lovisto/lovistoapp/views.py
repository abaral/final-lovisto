from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, logout, authenticate
from django.template import  loader

from . import xml_parser, json_parser, html_parser
from .models import Aportacion, Comentario
from django.contrib.auth.models import User

modo = 'Light'

def get_sitio_y_recurso(enlace):
    if enlace[0:7] == 'http://':
        enlace = enlace[7:]
    elif enlace[0:8] == 'https://':
        enlace = enlace[8:]
    if enlace[0:4] == 'www.':
        enlace = enlace[4:]
    if '/' in enlace:
        sitio = enlace.split('/', 1)[0]
        recurso = enlace.split('/', 1)[1]
    else:
        sitio = enlace
        recurso = None
    return sitio, recurso


def get_info_extend(sitio, recurso):
    pageid = ''
    sitios_reconocidos = ['aemet.es', 'es.wikipedia.org', 'youtube.com', 'reddit.com']
    if sitio in sitios_reconocidos:
        if sitio == 'aemet.es':
            pag = 'aemet'
            localidad_id = recurso.split('/')[4]
            localidad_id = localidad_id.split('-')[1]
            localidad_id = localidad_id.split('d')[1]
            url_xml = 'https://www.aemet.es/xml/municipios/localidad_' + localidad_id + '.xml'
            info_extend = xml_parser.main(url_xml, pag)
        elif sitio == 'es.wikipedia.org':
            pag = 'wiki'
            articulo = recurso.split('/')[1]
            url_xml = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=' + articulo + \
                      '&prop=extracts&exintro&explaintext'
            url_json = 'https://es.wikipedia.org/w/api.php?action=query&titles=' + articulo + \
                       '&prop=pageimages&format=json&pithumbsize=100'
            info_extend_xml, pageid = xml_parser.main(url_xml, pag)
            info_extend_json = json_parser.main(url_json, pag, pageid)
            info_extend = info_extend_xml + info_extend_json
        elif sitio == 'youtube.com':
            pag = 'yt'
            video_id = recurso.split('=')[1]
            url_json = 'https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=' + video_id
            info_extend = json_parser.main(url_json, pag, video_id)
    else:
        url = 'http://' + str(sitio) + '/' + str(recurso)
        info_extend = html_parser.main(url)

    return info_extend


def recent_aports():
    aports_list = Aportacion.objects.all()
    aports_list = sorted(aports_list, key=lambda aportacion: aportacion.fecha, reverse=True)
    last_aports = aports_list[0:3]
    return last_aports


def reconocer_query_string(request):
    format_dwld = request.GET.get('format')
    aports_user_list = {}
    aports_list = Aportacion.objects.all()
    aports_list = sorted(aports_list, key=lambda aportacion: aportacion.fecha, reverse=True)
    ten_aports_list = aports_list[0:10]
    if request.user.is_authenticated:
        aports_user_list = Aportacion.objects.filter(autor=request.user)
        aports_user_list = sorted(aports_user_list, key=lambda aportacion: aportacion.fecha,
                                  reverse=True)[0:5]
    context = {'aports_list': aports_list,
               'ten_aports_list': ten_aports_list,
               'aports_user_list': aports_user_list,
               'recent_aports': recent_aports(),
               'options': [True, False, False, False, False],
               'voto_opcion': ['L', 'D', 'N'],
               'modo': modo}
    if format_dwld == 'xml':
        return render(request, 'lovisto/all_aports.xml', context, content_type="text/xml")
    elif format_dwld == 'json':
        return render(request, 'lovisto/all_aports.json', context, content_type="text/json")
    if format_dwld is None:
        return render(request, 'lovisto/index.html', context)


def manage_request(request):
    global modo
    if request.method == 'POST':
        action = request.POST['action']
        if action == 'Login':
            login_view(request)
        elif action == 'Create an account':
            username = request.POST['user']
            password = request.POST['password']
            user = User.objects.create_user(username=username, password=password)
            user.save()
            login(request, user)
        elif action == 'Enviar aportacion':
            titulo = request.POST['titulo']
            enlace = request.POST['enlace']
            sitio, recurso = get_sitio_y_recurso(enlace)
            url = 'http://' + str(sitio) + '/' + str(recurso)
            descripcion = request.POST['descripcion']
            autor = request.user
            fecha = timezone.now()
            extend_info = get_info_extend(sitio, recurso)
            aportacion = Aportacion(titulo=titulo, enlace=url, descripcion=descripcion, autor=autor,
                                    fecha=fecha, extend_info=extend_info)
            aportacion.save()
        elif action == 'Enviar comentario':
            id_aportacion = request.POST.get('aportacion')
            aportacion = Aportacion.objects.get(id=id_aportacion)
            cuerpo = request.POST['cuerpo']
            autor = request.user
            fecha = timezone.now()
            aportacion.num_coments = aportacion.num_coments + 1
            comentario = Comentario(aportacion=aportacion, cuerpo=cuerpo, autor=autor, fecha=fecha)
            aportacion.save()
            comentario.save()
        elif action == 'Like':
            id_aportacion = request.POST.get('aportacion')
            aportacion = Aportacion.objects.get(id=id_aportacion)
            if aportacion.dislikes.filter(id=request.user.id).exists():
                aportacion.dislikes.remove(request.user)
                aportacion.likes.add(request.user)
                aportacion.voto = 'L'
                aportacion.save()
            elif not aportacion.likes.filter(id=request.user.id).exists():
                aportacion.likes.add(request.user)
                aportacion.voto = 'L'
                aportacion.save()
        elif action == 'Dislike':
            id_aportacion = request.POST.get('aportacion')
            aportacion = Aportacion.objects.get(id=id_aportacion)
            if aportacion.likes.filter(id=request.user.id).exists():
                aportacion.likes.remove(request.user)
                aportacion.dislikes.add(request.user)
                aportacion.voto = 'D'
                aportacion.save()
            elif not aportacion.dislikes.filter(id=request.user.id).exists():
                aportacion.dislikes.add(request.user)
                aportacion.voto = 'D'
                aportacion.save()
        elif action == 'Light':
            modo = 'Light'
        elif action == 'Dark':
            modo = 'Dark'
    return None

@csrf_exempt
def index(request):
    manage_request(request)
    return reconocer_query_string(request)

@csrf_exempt
def all_aportaciones(request):
    manage_request(request)
    aports_list = Aportacion.objects.all()
    aports_list = sorted(aports_list, key=lambda aportacion: aportacion.fecha, reverse=True)
    context = {'aports_list': aports_list,
               'recent_aports': recent_aports(),
               'options': [False, False, True, False, False],
               'modo': modo}
    return render(request, 'lovisto/aports.html', context)

@csrf_exempt
def author_info(request):
    manage_request(request)
    context = {'recent_aports': recent_aports(),
               'options': [False, True, False, False, False],
               'modo': modo}
    return render(request, 'lovisto/author_info.html', context)


def banner(request):
    context = {}
    return render(request, 'lovisto/banner.html', context)

@csrf_exempt
def get_aportacion(request, id_aportacion):
    manage_request(request)
    aportacion = Aportacion.objects.get(id=id_aportacion)
    comentarios_list = Comentario.objects.filter(aportacion=aportacion)
    pos_votos = aportacion.likes.count()
    neg_votos = aportacion.dislikes.count()
    context = {'aportacion': aportacion,
               'comentarios_list': comentarios_list,
               'recent_aports': recent_aports(),
               'pos_votos': pos_votos,
               'neg_votos': neg_votos,
               'modo': modo}
    return render(request, 'lovisto/pag_aportacion.html', context)

@csrf_exempt
def user_page(request, id_user):
    manage_request(request)
    user = User.objects.get(id=id_user)
    aports_list = Aportacion.objects.all()
    aports_user_list = Aportacion.objects.filter(autor=user)
    comentarios_list = Comentario.objects.filter(autor=user)
    for aport in aports_list:
        if aport.likes.filter(id=id_user).exists():
            aport.voto = 'L'
        elif aport.dislikes.filter(id=id_user).exists():
            aport.voto = 'D'
        else:
            aport.voto = 'N'
    context = {'user': user,
               'aports_list': aports_list,
               'voto_opcion': ['L', 'D', 'N'],
               'aports_user_list': aports_user_list,
               'comentarios_list': comentarios_list,
               'recent_aports': recent_aports(),
               'modo': modo}
    return render(request, 'lovisto/pag_user.html', context)


def login_view(request):
    username = request.POST['user']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
    return redirect('/')


def logout_view(request):
    logout(request)
    return redirect('/')
