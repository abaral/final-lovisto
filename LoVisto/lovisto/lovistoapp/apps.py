from django.apps import AppConfig


class LovistoappConfig(AppConfig):
    name = 'lovistoapp'
