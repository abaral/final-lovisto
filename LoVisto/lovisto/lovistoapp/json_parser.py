import json
import urllib.request
import contextlib


def extract_data_wiki(json_data, pageid):
    data = {}
    data['imagen'] = json_data['query']['pages'][pageid]['thumbnail']['source']

    extend_info = '<div class=\"wiki\"><img src=\"' + str(data['imagen']) + \
                  '\" alt=\"Imagen wikipedia\"></div><br>'
    return extend_info


def extract_data_youtube(json_data, videoid):
    extend_info = {}
    extend_info['title'] = json_data['title']
    extend_info['autor'] = json_data['author_name']
    extend_info['video'] = json_data['html']

    src_video = extend_info['video'].split('src="')[1]
    src_video = src_video.split('"')[0]

    extend_info = '<div class=\"youtube\"><p>' + str(extend_info['title']) + \
                  '</p><iframe width=\"560\" height=\"315\" src=\"' + str(src_video) + \
                  '\" title=\"Youtube video player\" frameborder=\"0\" allow=\"accelerometer;' +\
                  'encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>' + \
                  '<p>Autor: ' + str(extend_info['autor']) + '. <a href="https://www.youtube.com/watch?v=' +\
                  str(videoid) + '\">Ver en Youtube</a></p></div>'
    return extend_info


def main(url_json, sitio, id):
    with contextlib.closing(urllib.request.urlopen(url_json)) as json_file:
        json_str = json_file.read().decode(encoding="ISO-8859-1")
        json_data = json.loads(json_str)
        if sitio == 'wiki':
            extend_info = extract_data_wiki(json_data, id)
        elif sitio == 'yt':
            extend_info = extract_data_youtube(json_data, id)
    return extend_info
