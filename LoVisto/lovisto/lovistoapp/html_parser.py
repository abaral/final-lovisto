from bs4 import BeautifulSoup
import urllib.request

def main(url):
    html_stream = urllib.request.urlopen(url)
    soup = BeautifulSoup(html_stream, 'html.parser')

    title = soup.find('meta', property='og:title')
    if title:
        title = title['content']
        info_extend = '<div class=\"og\"><p>Título: ' + title + '</p>'
        image = soup.find('meta', property='og:image')
        if image:
            image = image['content']
            info_extend = info_extend + \
                          '<img src=\"' + image + '\"></p></div>'
        else:
            info_extend = info_extend + '</div>'
    else:
        info_extend = '<div class=\"html\"><p>Info extendida no disponible</p></div>'

    return info_extend