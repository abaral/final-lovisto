from django.contrib import admin
from .models import Aportacion, Comentario


admin.site.register(Aportacion)
admin.site.register(Comentario)
