from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('aportaciones', views.all_aportaciones),
    path('about_author', views.author_info),
    path('banner', views.banner),
    path('logout', views.logout_view),
    path('aportacion_<id_aportacion>', views.get_aportacion),
    path('user_<id_user>', views.user_page),
]