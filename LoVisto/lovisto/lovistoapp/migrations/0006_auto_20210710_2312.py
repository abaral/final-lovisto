# Generated by Django 3.1.7 on 2021-07-10 23:12

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lovistoapp', '0005_auto_20210710_2309'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aportacion',
            name='neg_votos',
        ),
        migrations.RemoveField(
            model_name='aportacion',
            name='pos_votos',
        ),
        migrations.RemoveField(
            model_name='aportacion',
            name='votos',
        ),
        migrations.AddField(
            model_name='aportacion',
            name='dislikes',
            field=models.ManyToManyField(blank=True, related_name='dislikes', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='aportacion',
            name='likes',
            field=models.ManyToManyField(blank=True, related_name='likes', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='Voto',
        ),
    ]
